import {showModal} from './modal';
import {createElement} from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const { name, source: src } = fighter;
  const title = `Чемпіон - ${name}. Знову всім натовк пику`;
  const bodyElement = createElement({ tagName: 'img', className: 'modal-body', attributes: { src } });

  showModal({ title, bodyElement, onClose: () => document.location.reload() });
  // call showModal function  +
}
